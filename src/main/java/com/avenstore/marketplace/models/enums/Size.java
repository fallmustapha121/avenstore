package com.avenstore.marketplace.models.enums;

public enum Size {
    SMALL, MEDIUM, LARGE
}
